﻿
using API.Ruleta.Dtos;

namespace API.Ruleta.IServices
{
    public interface IBetService
    {
        string CreateBet(BetsRoulette betsRoulette);
    }
}
