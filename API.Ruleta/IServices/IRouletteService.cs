﻿using API.Ruleta.Dtos;
using API.Ruleta.Models;
using System.Collections.Generic;

namespace API.Ruleta.IServices
{
    public interface IRouletteService
    {
        IEnumerable<Roulette> GetRoulettes();
        int CreateRoulette();
        bool RouletteOpen(int Id);
        BetsRouletteClose RouletteNotOpen(int Id);
    }
}
