﻿namespace API.Ruleta.IServices
{
    public interface IUserService
    {
        int CreateUser(decimal credit);
        bool UpdateCreditUserLess(int Id,int IdRoulette,decimal Money);
        bool UpdateCreditUserSum(int Id, decimal EarnedMoney);
    }
}
