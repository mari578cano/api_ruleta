﻿using API.Ruleta.Dtos;
using API.Ruleta.IServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Ruleta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BetController : ControllerBase
    {
        private readonly IBetService _betService;
        public BetController(IBetService betService)
        {
            _betService = betService;
        }

        [HttpPost("CreateBet")]
        public ActionResult CreateBet(BetsRoulette betsRoulette)
        {
            string result = _betService.CreateBet(betsRoulette);
            return Ok(result);
        }
    }
}
