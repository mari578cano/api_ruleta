﻿using API.Ruleta.IServices;
using Microsoft.AspNetCore.Mvc;

namespace API.Ruleta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("CreateUser")]
        public ActionResult CreateRoulette(decimal credit)
        {
            int id = _userService.CreateUser(credit);
            return Ok(id);
        }
    }
}
