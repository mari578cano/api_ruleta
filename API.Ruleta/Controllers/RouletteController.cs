﻿using API.Ruleta.Dtos;
using API.Ruleta.IServices;
using API.Ruleta.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace API.Ruleta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RouletteController : ControllerBase
    {
        private readonly IRouletteService _rouletteService;
        public RouletteController(IRouletteService rouletteService) 
        {
            _rouletteService = rouletteService;
        }

        [HttpPost("CreateRoulette")]
        public ActionResult CreateRoulette()
        {
            int id = _rouletteService.CreateRoulette();
            return Ok(id);
        }

        [HttpPost("OpenRoulette")]
        public ActionResult OpenRoulette(int Id)
        {
            var result = _rouletteService.RouletteOpen(Id);
            return Ok(result);
        }

        [HttpPost("CloseRoulette")]
        public ActionResult CloseRoulette(int Id)
        {
            BetsRouletteClose result = _rouletteService.RouletteNotOpen(Id);
            if (result.IdRoulette <= 0)
                return BadRequest("No se cerro la ruleta");

            return Ok(result);
        }

        [HttpGet]
        public IEnumerable<Roulette> Get()
        {
            var result = _rouletteService.GetRoulettes();
            return result;
        }
    }
}
