﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Ruleta.Models
{
    public class Roulette
    {
        private int _Id;
        private bool _Active = false;
        private List<Bets> _Bets;

        public int Id
        {
            get { return _Id; }
            set  { _Id = value; }
        }

        public bool Active
        {
            get { return _Active; }
            set { _Active = value; }
        }
        public List<Bets> Bets
        {
            get { return _Bets; }
            set { _Bets = value; }
        }
    }
}
