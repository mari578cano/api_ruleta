﻿using Newtonsoft.Json;

namespace API.Ruleta.Models
{
    public class Bets
    {
        private int _IdUser;
        private decimal _Money;
        private int _Number;
        private string _Color;

        public int IdUser
        {
            get { return _IdUser; }
            set { _IdUser = value; }
        }
        public decimal Money
        {
            get { return _Money; }
            set { _Money = value; }
        }
        public int Number
        {
            get { return _Number; }
            set { _Number = value; }
        }
        public string Color
        {
            get { return _Color; }
            set { _Color = value; }
        }
    }
}
