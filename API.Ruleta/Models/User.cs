﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Ruleta.Models
{
    public class User
    {
        private int _Id;
        private decimal _Credit;

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
        public decimal Credit
        {
            get { return _Credit; }
            set { _Credit = value; }
        }
    }
}
