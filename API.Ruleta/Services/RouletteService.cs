﻿using API.Ruleta.Dtos;
using API.Ruleta.IRepositories;
using API.Ruleta.IServices;
using API.Ruleta.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace API.Ruleta.Services
{
    public class RouletteService : IRouletteService
    {
        private readonly IRouletteRepository _rouletteRepository;
        private readonly IUserService _userService;
        private readonly string key = "Roulette";
        public RouletteService(IRouletteRepository rouletteRepository, IUserService userService)
        {
            _rouletteRepository = rouletteRepository;
            _userService = userService;
        }

        public IEnumerable<Roulette> GetRoulettes()
        {
            var listRoulettes = _rouletteRepository.GetList(key);
            return listRoulettes;
        }

        public int CreateRoulette()
        {
            Roulette objRoulette = new Roulette();
            objRoulette.Id = GenereteId(key);
            var listRoulettes = _rouletteRepository.GetList(key);
            listRoulettes.Add(objRoulette);
            bool result = _rouletteRepository.Add(listRoulettes, key);
            if (!result)
                return 0;

            return objRoulette.Id;
        }

        public bool RouletteOpen(int Id) 
        {
            bool result = _rouletteRepository.OpenRoulette(Id, key);
            if (result)
                return true;
            return false;
        }

        private int GenereteId(string key)
        {
            List<Roulette> list = _rouletteRepository.GetList(key);
            if (list.Count <= 0)
                return 0 + 1;
            else
                return list.Last().Id + 1;
        }

        public BetsRouletteClose RouletteNotOpen(int Id)
        {
            BetsRouletteClose betsRouletteClose = new BetsRouletteClose();
            if (_rouletteRepository.IsOpenRoulette(Id, key)) 
            {
                bool resultCloseRoulette = _rouletteRepository.OpenNotRoulette(Id, key);
                if (resultCloseRoulette) 
                {
                    decimal SumTotalBets = SumBets(Id);
                    (int Number, string Color) resultNumberColorWin = NumberRandom();
                    List<string> resultListUser = CalculateEarnedValue(Id, resultNumberColorWin.Number, resultNumberColorWin.Color);
                    return SetBetsRouletteClose(Id, resultNumberColorWin.Number, resultNumberColorWin.Color, SumTotalBets, resultListUser);
                }
            }

            return betsRouletteClose;
        }

        private decimal SumBets(int Id)
        {
            Roulette roulette = _rouletteRepository.GetRouletteById(Id,key);
            decimal sumBets = 0;
            foreach (Bets bet in roulette.Bets)
            {
                sumBets = sumBets + bet.Money;
            }

            return sumBets;
        }

        private (int Number,string Color) NumberRandom()
        {
            Random random = new Random();
            int Number = random.Next(0, 36);
            string Color = string.Empty;
            if (Number % 2 == 0)
                Color = "Rojo";
            else
                Color = "Negro";

            return (Number,Color);

        }

        private List<string> CalculateEarnedValue(int IdRoulette,int EarnedNumber,string EarnedColor)
        {
            Roulette roulette = _rouletteRepository.GetRouletteById(IdRoulette, key);
            List<string> ListUserMoneyWin = new List<string>();
            foreach (Bets bet in roulette.Bets)
            {
                decimal SumEarned = 0;
                if (bet.Number == EarnedNumber)
                    SumEarned = bet.Money * 5;
                if (bet.Color == EarnedColor)
                    SumEarned = bet.Money * (decimal)1.8;
                if (SumEarned > 0)
                {
                    _userService.UpdateCreditUserSum(bet.IdUser, SumEarned);
                    string ConcateDataUserMoneyWin = "User: " + bet.IdUser + "MoneyEarned: " + SumEarned;
                    ListUserMoneyWin.Add(ConcateDataUserMoneyWin);
                }      
            }

            return ListUserMoneyWin;
        }

        private BetsRouletteClose SetBetsRouletteClose(int IdRoulette,int NumberEarned, string ColorEarned,decimal SumTotalEarned,List<string> UsersMoney)
        {
            BetsRouletteClose betsRouletteClose = new BetsRouletteClose();
            betsRouletteClose.IdRoulette = IdRoulette;
            betsRouletteClose.Number = NumberEarned;
            betsRouletteClose.Color = ColorEarned;
            betsRouletteClose.SumMoneyBet = SumTotalEarned;
            betsRouletteClose.UsersAndMoneyEarned = UsersMoney;
            return betsRouletteClose;
        }

    }
}
