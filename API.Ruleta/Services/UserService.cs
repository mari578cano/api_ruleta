﻿using API.Ruleta.IRepositories;
using API.Ruleta.IServices;
using API.Ruleta.Models;
using System.Collections.Generic;
using System.Linq;

namespace API.Ruleta.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly string key = "User";
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public int CreateUser(decimal credit)
        {
            User objUser = new User();
            objUser.Id = GenereteId(key);
            objUser.Credit = credit;
            var listUsers = _userRepository.GetList(key);
            listUsers.Add(objUser);
            bool result = _userRepository.Add(listUsers, key);
            if (!result)
                return 0;

            return objUser.Id;
        }

        private int GenereteId(string key)
        {
            List<User> list = _userRepository.GetList(key);
            if (list.Count <= 0)
                return 0 + 1;
            else
                return list.Last().Id + 1;
        }

        public bool UpdateCreditUserLess(int Id,int IdRoulette,decimal Money)
        {
            User objUser = new User();
            objUser.Id = Id;
            objUser.Credit = objUser.Credit - Money;
            var listUsers = _userRepository.GetList(key);
            listUsers.Add(objUser);
            bool result = _userRepository.Add(listUsers, key);
            if (!result)
                return false;
            return true;
        }

        public bool UpdateCreditUserSum(int Id, decimal EarnedMoney)
        {
            User objUser = new User();
            objUser.Id = Id;
            objUser.Credit = objUser.Credit + EarnedMoney;
            var listUsers = _userRepository.GetList(key);
            listUsers.Add(objUser);
            bool result = _userRepository.Add(listUsers, key);
            if (!result)
                return false;
            return true;
        }
    }
}

