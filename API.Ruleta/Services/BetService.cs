﻿using API.Ruleta.Dtos;
using API.Ruleta.IRepositories;
using API.Ruleta.IServices;
using API.Ruleta.Models;
using System.Collections.Generic;
using System.Linq;

namespace API.Ruleta.Services
{
    public class BetService : IBetService
    {
        private readonly IBetRepository _betRepository;
        private readonly IRouletteRepository _rouletteRepository;
        private readonly IUserService _userService;
        private readonly string key = "Roulette";
        public BetService(IBetRepository betRepository, IRouletteRepository rouletteRepository, IUserService userService)
        {
            _betRepository = betRepository;
            _rouletteRepository = rouletteRepository;
            _userService = userService;
        }

        public string CreateBet(BetsRoulette betsRoulette)
        {
            if (_rouletteRepository.IsOpenRoulette(betsRoulette.IdRoulette, key))
            {
                decimal SumBetsOpen = ValidateAndAddBetsByUserRoulette(betsRoulette.IdRoulette);

                if (!CanUserBets(SumBetsOpen, betsRoulette.Money))
                    return "Valor apostado supera el monto permitido en total";
                bool resultRoulette = _rouletteRepository.Add(SetRouletteandBets(betsRoulette, SumBetsOpen), key);
                bool resultUser = _userService.UpdateCreditUserLess(betsRoulette.IdUser, betsRoulette.IdRoulette,betsRoulette.Money);
                if (!resultRoulette)
                    return "No se guardo la apuesta";
                else if (!resultUser)
                    return "No cuenta con el dinero necesario para apostar";

                return "Exitoso";
            }

            return "Ruleta no abierta";
        }

        private decimal ValidateAndAddBetsByUserRoulette(int Id) 
        {
            Roulette obj = _rouletteRepository.GetRouletteById(Id, key);
            decimal sumBetsOpen = 0;
            if (obj.Bets != null)
            {
                foreach (var item in obj.Bets) {
                    sumBetsOpen = sumBetsOpen + item.Money;
                }
            }
            else
                return 0;

            return sumBetsOpen;
            
        }

        private bool CanUserBets(decimal SumBetOpen, decimal Money)
        {
            decimal sumTotalBets = SumBetOpen + Money;
            if (sumTotalBets > 10000)
                return false;
            else
                return true;
        }

        private List<Roulette> SetRouletteandBets(BetsRoulette betsRoulette, decimal SumBetsOpen)
        {
            List<Roulette> listRoulettes = _rouletteRepository.GetList(key);
            int indexRoulette = listRoulettes.IndexOf(listRoulettes.FirstOrDefault(x => x.Id == betsRoulette.IdRoulette));
            Roulette roulette = listRoulettes[indexRoulette];
            roulette.Bets = SetObjBets(betsRoulette, SumBetsOpen);
            return listRoulettes;
        }

        private List<Bets> SetObjBets(BetsRoulette betsRoulette,decimal SumBetsOpen) 
        {
            List<Bets> Listbets = new List<Bets>();
            Bets objBet = new Bets();
            objBet.IdUser = betsRoulette.IdUser;
            objBet.Number = betsRoulette.Number;
            objBet.Color = betsRoulette.Color;
            objBet.Money = betsRoulette.Money + SumBetsOpen;
            Listbets.Add(objBet);
            return Listbets;
        }
    }
}
