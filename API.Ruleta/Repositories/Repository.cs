﻿using API.Ruleta.Helpers;
using API.Ruleta.IRepositories;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;

namespace API.Ruleta.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        public Repository(IConnectionMultiplexer multiplexer) 
        {
            _connectionMultiplexer = multiplexer;
        }

        public bool Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public List<T> GetList(string key)
        {
            IDatabase db = _connectionMultiplexer.GetDatabase(3);
            string listString = db.StringGet(key);
            if (listString == null)
                return new List<T>();

            return JsonConvert.DeserializeObject<List<T>>(listString);
        }

        public bool Add(List<T> entity, string key)
        {
            IDatabase db = _connectionMultiplexer.GetDatabase(3);
            db.StringSet(key, JsonConvert.SerializeObject(entity));

            return true;
        }

        public bool Update(T entity, string key)
        {
            throw new NotImplementedException();
        }
    }
}
