﻿using API.Ruleta.IRepositories;
using API.Ruleta.Models;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Ruleta.Repositories
{
    public class BetRepository : Repository<Bets>, IBetRepository
    {
        public BetRepository(IConnectionMultiplexer multiplexer) : base(multiplexer)
        {
        }
    }
}
