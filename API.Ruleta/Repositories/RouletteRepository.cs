﻿using API.Ruleta.IRepositories;
using API.Ruleta.Models;
using StackExchange.Redis;
using System.Collections.Generic;
using System.Linq;

namespace API.Ruleta.Repositories
{
    public class RouletteRepository : Repository<Roulette>, IRouletteRepository
    {
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        public RouletteRepository(IConnectionMultiplexer connectionMultiplexer) : base(connectionMultiplexer)
        {
            _connectionMultiplexer = connectionMultiplexer;
        }

        public Roulette GetRouletteById(int Id, string key)
        {
            IDatabase db = _connectionMultiplexer.GetDatabase(3);
            List<Roulette> list = GetList(key);
            int indexRoulette = list.IndexOf(list.FirstOrDefault(x => x.Id == Id));
            Roulette roulette = list[indexRoulette];

            return roulette;
        }

        public bool OpenRoulette(int Id, string key)
        {
            IDatabase db = _connectionMultiplexer.GetDatabase(3);
            List<Roulette> list = GetList(key);
            int indexRoulette = list.IndexOf(list.FirstOrDefault(x => x.Id == Id));
            list[indexRoulette].Active = true;
            Add(list, key);

            return true;
            
        }

        public bool IsOpenRoulette(int Id, string key)
        {
            IDatabase db = _connectionMultiplexer.GetDatabase(3);
            List<Roulette> list = GetList(key);
            int indexRoulette = list.IndexOf(list.FirstOrDefault(x => x.Id == Id));
            if (list[indexRoulette].Active)
                return true;
            return false;
        }

        public bool OpenNotRoulette(int Id, string key)
        {
            IDatabase db = _connectionMultiplexer.GetDatabase(3);
            List<Roulette> list = GetList(key);
            int indexRoulette = list.IndexOf(list.FirstOrDefault(x => x.Id == Id));
            list[indexRoulette].Active = false;
            Add(list, key);

            return true;

        }
    }
}
