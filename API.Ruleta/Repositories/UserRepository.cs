﻿using API.Ruleta.IRepositories;
using API.Ruleta.Models;
using StackExchange.Redis;

namespace API.Ruleta.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        public UserRepository(IConnectionMultiplexer connectionMultiplexer) : base(connectionMultiplexer)
        {
            _connectionMultiplexer = connectionMultiplexer;
        }
    }
}
