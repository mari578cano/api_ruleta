﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
namespace API.Ruleta.Helpers
{
    public class Redis
    {
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        public  Redis(IConnectionMultiplexer multiplexer)
        {
            _connectionMultiplexer = multiplexer;
        }

        public IDatabase ConnectionRedis()
        {
            var database = _connectionMultiplexer.GetDatabase();
            return database;
        }
    }
}
