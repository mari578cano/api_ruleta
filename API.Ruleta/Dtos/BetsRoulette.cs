﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Ruleta.Dtos
{
    public class BetsRoulette
    {
        public int IdRoulette { get; set; }
        public int IdUser { get; set; }
        public int Number { get; set; }
        public string Color { get; set; }
        public decimal Money { get; set; }
    }
}
