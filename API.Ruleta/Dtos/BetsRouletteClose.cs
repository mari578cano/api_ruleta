﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Ruleta.Dtos
{
    public class BetsRouletteClose
    {
        public int IdRoulette { get; set; }
        public int Number { get; set; }
        public string Color { get; set; }
        public decimal SumMoneyBet { get; set; }
        public List<string> UsersAndMoneyEarned { get; set; }
    }
}
