﻿using API.Ruleta.Models;

namespace API.Ruleta.IRepositories
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
