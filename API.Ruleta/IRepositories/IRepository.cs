﻿using System.Collections.Generic;
using System.Linq;

namespace API.Ruleta.IRepositories
{
    public interface IRepository<T> where T:class
    {
        bool Delete(T entity);
        bool Update(T entity, string key);
        bool Add(List<T> entity, string key);
        List<T> GetList(string key);

    }
}
