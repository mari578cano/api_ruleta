﻿using API.Ruleta.Models;

namespace API.Ruleta.IRepositories
{
    public interface IRouletteRepository : IRepository<Roulette>
    {
        Roulette GetRouletteById(int Id, string Key);

        bool OpenRoulette(int Id,string key);

        bool IsOpenRoulette(int Id, string key);

        bool OpenNotRoulette(int Id, string key);
    }
}
